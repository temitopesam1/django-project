from django.contrib import admin
from .models import Connection, ChatMessage



admin.site.register(Connection)
admin.site.register(ChatMessage)