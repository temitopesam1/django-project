from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json
from websocket.models import ChatMessage, Connection
import boto3

@csrf_exempt
def test(request):
	context = {'message': 'hello Daud'}
	return JsonResponse( context, status=200)

def _parse_body(body):
	body_unicode = body.decode('utf-8')
	return json.loads(body_unicode)


@csrf_exempt
def connect(request):
	body = _parse_body(request.body)
	Connection.objects.create(connection_id = body['connectionId'])
	context = {'message': 'connect successfully'}
	return JsonResponse( context, status=200)


@csrf_exempt
def disconnect(request):
	body = _parse_body(request.body)
	connection_id = body['connectionId']
	disconnected = Connection.objects.get(connection_id = connection_id)
	disconnected.delete()
	return JsonResponse({'message': 'disconnected successfully'}, status=200)


def _send_to_connection(connection_id, data):
	gatewayapi=boto3.client('apigatewaymanagementapi', endpoint_url= 'https://jfzhgq5o3l.execute-api.us-east-2.amazonaws.com/Test-Stage/'
, region_name= 'us-east-2',  aws_access_key_id = 'AKIA6FTDMYEGNJKCYV5M', aws_secret_access_key = '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')

	return gatewayapi.post_to_connection(ConnectionId=connection_id,  Data=json.dumps(data).encode('utf-8'))


@csrf_exempt
def send_message(request):
	body = _parse_body(request.body)
	content = body['content']
	username = body['username']
	timestamp = body['timestamp']
	chat = ChatMessage(message = content, username = username, timestamp = timestamp)

	chat.save()
	connections = Connection.objects.all()

	data = {'messages':[body]}

	for connection in connections:
		print(connection)

		_send_to_connection(connection.connection_id, data)

	return JsonResponse({'message': 'message sent to all client successfully'}, status=200)

@csrf_exempt
def get_recent_messages(request):

	messages = []
	chats = ChatMessage.objects.all().order_by('-id')
	for i in range(len(chats)):
		data = {
		'username': chats[i].username,
		'message': chats[i].message,
		'timestamp': chats[i].timestamp
		}

		messages.append(data)
	data = {'messages': messages}
	for connection in Connection.objects.all():
		_send_to_connection(connection.connection_id, data)
	return JsonResponse(data, status=200)

	
