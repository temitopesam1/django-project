from django.apps import AppConfig


class Temitopesam5ScrumyapiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'temitopesam5scrumyapi'
