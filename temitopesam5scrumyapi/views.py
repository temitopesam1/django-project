from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets

from .serializers import ScrumyGoalsApiSerializer
from .models import ScrumyGoalsApi


class ScrumyGoalsViewSet(viewsets.ModelViewSet):
    queryset = ScrumyGoalsApi.objects.all().order_by('goal_name')
    serializer_class = ScrumyGoalsApiSerializer