from django.contrib import admin
from .models import GoalStatus
from .models import ScrumyHistory
from .models import ScrumyGoals

admin.site.register(GoalStatus)
admin.site.register(ScrumyGoals)
admin.site.register(ScrumyHistory)

# Register your models here.
