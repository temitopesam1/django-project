from django.db import models
from django.contrib.auth.models import user


class ScrumyGoals(models.Model):
	goal_name = models.CharField(max_length = 200)
	goal_id = models.CharField(max_length = 200)
	created_by = models.CharField(max_length = 200)
	moved_by = models.CharField(max_length = 200)
	owner = models.CharField(max_length = 200)
	goal_status = models.ForeignKey(GoalStatus, on_delete = models.PROTECT)
	user = models.ForeignKey(user, related_name = 'GoalStatus', on_delete=models.DO_NOTHING)
	
    def __str__(self):
    	return self.goal_name

class ScrumyHistory(models.Model):
	moved_by = models.CharField(max_length = 200)
	created_by = models.CharField(max_length = 200)
	moved_from = models.CharField(max_length = 200)
	moved_to = models.CharField(max_length = 200)
	time_of_action = DateTimeField()
	goal = models.ForeignKey(ScrumyGoals, on_delete=models.DO_NOTHING)



class GoalStatus(models.Model):
	status_name = models.CharField(max_length = 200)
	content = models.TextField()
	goal_status = models.ForeignKey(ScrumyGoals, to_field='goal_status', on_delete=models.DO_NOTHING)
# Create your models here.
