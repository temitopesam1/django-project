------------------
temitopesam5scrumy
------------------


Quick start
-----------

1. add 'temitopesam5scrumy' to the INSTALLED_APPS settings like this ::

  INSTALLED_APPS =[
     ---
     'temitopesam5scrumy',
  ]

2. include the temitopesam5scrumy URLconfig in your project urls.py like this ::

   path('temitopesam5scrumy/', include('temitopesam5scrumy.urls')),

3. run 'py manage.py migrate' to create the temitopesam5scrumy models.

4. start the development server and visit 'http://127.0.0.1:8000/admin/'
   to create a temitopesam5scrumy(you'll need the admin app enabled).

5. visit http://127.0.0.1:8000/temitopesam5scrumy to check the software out.
 