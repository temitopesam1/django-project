from django.shortcuts import render
from .models import ScrumyGoals, ScrumyHistory, GoalStatus
from django.contrib.auth.models import User

import random

from django.http import HttpResponse 


def index(request):

	return HttpResponse(ScrumyGoals.objects.filter(goal_name='Learn Django'))

def move_goal(request,goal_id):

	try:

		obj = ScrumyGoals.objects.get(goal_id=goal_id) 

	except Exception as e:

		return render(request, 'temitopesam5scrumy/exception.html', {'error': 'A record with that goal id does not exist'}) 

	else: 

		return HttpResponse(obj.goal_name)

	#id = ScrumyGoals.objects.get(goal_id=goal_id)

	#return HttpResponse(id.goal_name)

def add_goal(request):

	myuser = User.objects.get(username='louis')
	weekly_goal = GoalStatus.objects.get(status_name='Weekly Goals')


	ScrumyGoals.objects.create(goal_name='Keep Learning Django', goal_id=random.randint(1000,9999), created_by='Louis', moved_by='Louis', owner='Louis', goal_status=weekly_goal, user=myuser)

	return HttpResponse(ScrumyGoals.objects.filter(goal_name='Keep Learning Django'))

def home(request):

	#user = User.objects.get(username='louis')

	#context = {'context': ScrumyGoals.objects.filter(goal_name='Learn Django')}
	users = User.objects.all()

	goalstatus = GoalStatus.objects.get(status_name='Weekly Goals')
	weeklyrelatedgoals = goalstatus.scrumygoals_set.all()

	goalstatus = GoalStatus.objects.get(status_name='Daily Goals')
	dailyrelatedgoals = goalstatus.scrumygoals_set.all()

	goalstatus = GoalStatus.objects.get(status_name='Verify Goals')
	verifyrelatedgoals = goalstatus.scrumygoals_set.all()

	goalstatus = GoalStatus.objects.get(status_name='Done Goals')
	donerelatedgoals = goalstatus.scrumygoals_set.all()



	context = { 'related_weekly_goal':weeklyrelatedgoals,'related_daily_goal':dailyrelatedgoals, 'related_verify_goal':verifyrelatedgoals, 'related_done_goal':donerelatedgoals, 'users':users }

	return render(request, 'temitopesam5scrumy/home.html', context)


	


	
