
from django.urls import include, path
from . import views

#app_name = 'temitopesam5scrumy'

urlpatterns = [
   path('',views.index, name = 'index'),
   path('success/', views.success, name="success"),
   path('movegoal/<int:goal_id>', views.move_goal, name='movegoal'),
   path('addgoal/',views.add_goal, name='addgoal'),
   path('home/',views.home, name='home'),
   path('accounts/', include('django.contrib.auth.urls')),

]