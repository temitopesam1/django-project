

from django.shortcuts import render, redirect
from .models import ScrumyGoals, ScrumyHistory, GoalStatus
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from temitopesam5scrumy.form import SignUpForm, CreateGoalForm, MoveGoalForm
from django.contrib.auth.models import Group
import random
from django.http import HttpResponse 
from django.contrib.auth.decorators import login_required






def success(request):
    context = {'success_message':"Your account has been created successfully"}
    return render(request, 'temitopesam5scrumy/success.html', context)



def index(request):

    if request.method == 'POST':

        form = SignUpForm(request.POST)
        if form.is_valid():

        	my_group = Group.objects.get(name = 'Developer')
        	myuser = form.save()
        	my_group.user_set.add(myuser)
       		username = form.cleaned_data.get('username')
       		password = form.cleaned_data.get('password1')
       		user = authenticate(username=username, password=password)
       		login(request, user)
       		return render(request, 'temitopesam5scrumy/success.html', {'success_message': 'Your account has been created successfully'})
    else:
    	form = SignUpForm()
    	return render(request, 'temitopesam5scrumy/signup.html', {'form': form})








@login_required
def move_goal(request, goal_id):
    target_goal = ScrumyGoals.objects.get(goal_id=goal_id)
    old_goal_status = target_goal.goal_status.status_name
    current_user = request.user
    form = MoveGoalForm()
    if request.method == "POST":
        form = MoveGoalForm(request.POST, instance=target_goal)
        if form.is_valid():
            # Developer
            if current_user.groups.filter(name='Developer').exists():
                if current_user.username == target_goal.owner and old_goal_status != 'Done Goal':
                    form.save()
                    return render(request, 'temitopesam5scrumy/success.html', {'success_message': 'Update Successful'})

                else:
                    goal = ScrumyGoals.objects.get(goal_id=goal_id)
                    goal.save()
                    form = MoveGoalForm()
                    context = {
                        'form':form,
                        'goal_name':goal,
                        'error_message':'You can only move goals you created. You also do not have permission to set a goal as Done Goal'
                    }
                    return render(request, 'temitopesam5scrumy/movegoal.html', context)

            # Quality Assurance
            elif current_user.groups.filter(name='Quality Assurance').exists():
                if current_user.username == target_goal.owner:
                    form.save()
                    return render(request, 'temitopesam5scrumy/success.html', {'success_message': 'Update Successful'})

                elif old_goal_status == 'Verify Goal' or old_goal_status == 'Done Goal':
                    form.save()
                    return render(request, 'temitopesam5scrumy/success.html', {'success_message': 'Update Successful'})

                else:
                    goal = ScrumyGoals.objects.get(goal_id=goal_id)
                    goal.save()
                    form = MoveGoalForm()
                    context = {
                        'form': form,
                        'goal_name': goal,
                        'error_message': 'You can only move goals from Verify To Done if they were not created by you'
                    }
                    return render(request, 'temitopesam5scrumy/movegoal.html', context)



            # Owner
            elif current_user.groups.filter(name='Owner').exists():
                if current_user.username == target_goal.owner: 
                    form.save()
                    return render(request, 'temitopesam5scrumy/success.html',  {'success_message': 'Your goal has been updated successfully'})
                else:
                    goal = ScrumyGoals.objects.get(goal_id=goal_id)
                    goal.save()
                    form = MoveGoalForm()
                    context = {
                        'form': form,
                        'goal_name': goal,
                        'error_message': 'You do not have the permission to move this goal because you belong to the Owner group and did not create this goal'
                    }
                    return render(request, 'temitopesam5scrumy/movegoal.html', context)

            # Admin
            elif current_user.groups.filter(name='Admin').exists():
                form.save()
                return render(request, 'temitopesam5scrumy/success.html',  {'success_message': 'Your goal has been updated successfully'})
            else:
                goal = ScrumyGoals.objects.get(goal_id=goal_id)
                goal.save()
                form = MoveGoalForm()
                context = {
                    'form': form,
                    'goal_name': goal,
                    'error_message': 'You do not belong to any group'
                }
                return render(request, 'temitopesam5scrumy/movegoal.html', context)


    else:
        goal = ScrumyGoals.objects.get(goal_id=goal_id)
        goal.save()
        context = {'form': form, 'goal_name':goal, 'user': request.user}
        return render(request, 'temitopesam5scrumy/movegoal.html', context)




	#try:

	#	obj = ScrumyGoals.objects.get(goal_id=goal_id) 

	#except Exception as e:

	#	return render(request, 'temitopesam5scrumy/exception.html', {'error': 'A record with that goal id does not exist'}) 

	#else: 

	#	return HttpResponse(obj.goal_name)

	#id = ScrumyGoals.objects.get(goal_id=goal_id)

	#return HttpResponse(id.goal_name)



@login_required
def add_goal(request):

	'''myuser = User.objects.get(username='louis')
	weekly_goal = GoalStatus.objects.get(status_name='Weekly Goals')


	ScrumyGoals.objects.create(goal_name='Keep Learning Django', goal_id=random.randint(1000,9999), created_by='Louis', moved_by='Louis', owner='Louis', goal_status=weekly_goal, user=myuser)

	return HttpResponse(ScrumyGoals.objects.filter(goal_name='Keep Learning Django'))'''

	weekly_goals = GoalStatus.objects.get(status_name='Weekly Goals')
	#daily_goals = GoalStatus.objects.get(status_name='Daily Goals')
	#goals = [weekly_goals, daily_goals]
	form = CreateGoalForm()
	if request.method == 'POST':
		form = CreateGoalForm(request.POST)
		if form.is_valid():
			f =form.save(commit = False)
			user = form.cleaned_data['user']
			myuser = User.objects.get(username=user)
			f.goal_id = random.randint(1000,9999)
			f.goal_status = weekly_goals
			f.created_by = myuser
			f.moved_by = myuser
			f.owner = myuser
			f.save()
			return render(request, 'temitopesam5scrumy/success.html', {'success_message': 'Your account has been created successfully'})

	else:
		form = CreateGoalForm(initial={'user':request.user, })
		form.fields['user'].disabled = True
	return render(request, 'temitopesam5scrumy/addgoal.html', {'form': form, 'user': request.user})


def home(request):

	#user = User.objects.get(username='louis')

	#context = {'context': ScrumyGoals.objects.filter(goal_name='Learn Django')}
	users = User.objects.all()

	goalstatus = GoalStatus.objects.get(status_name='Weekly Goals')
	weeklyrelatedgoals = goalstatus.scrumygoals_set.all()

	goalstatus = GoalStatus.objects.get(status_name='Daily Goals')
	dailyrelatedgoals = goalstatus.scrumygoals_set.all()

	goalstatus = GoalStatus.objects.get(status_name='Verify Goals')
	verifyrelatedgoals = goalstatus.scrumygoals_set.all()

	goalstatus = GoalStatus.objects.get(status_name='Done Goals')
	donerelatedgoals = goalstatus.scrumygoals_set.all()



	context = { 'related_weekly_goal':weeklyrelatedgoals,'related_daily_goal':dailyrelatedgoals, 'related_verify_goal':verifyrelatedgoals, 'related_done_goal':donerelatedgoals, 'users':users }

	return render(request, 'temitopesam5scrumy/home.html', context)


	


	
