from django.db import models
from django.contrib.auth.models import User

class GoalStatus(models.Model):
	status_name = models.CharField(max_length = 200)

	def __str__(self):
		return self.status_name

	class Meta:
		verbose_name_plural = "GoalStatus"

class ScrumyGoals(models.Model):
	goal_name = models.CharField(max_length = 200)
	goal_id = models.CharField(max_length = 200)
	created_by = models.CharField(max_length=200)
	moved_by = models.CharField(max_length=200)
	owner = models.CharField(max_length=200)
	goal_status = models.ForeignKey(GoalStatus, on_delete=models.PROTECT)
	user = models.ForeignKey(User, related_name='user', on_delete=models.DO_NOTHING)

	def __str__(self):
		return self.goal_name

	class Meta:
		verbose_name_plural = "ScrumyGoals"


class ScrumyHistory(models.Model):
	moved_by = models.CharField(max_length=200)
	created_by = models.CharField(max_length=200)
	moved_from = models.CharField(max_length=200)
	moved_to = models.CharField(max_length=200)
	time_of_action = models.DateTimeField()
	goal = models.ForeignKey(ScrumyGoals, on_delete=models.DO_NOTHING)

	def __str__(self):
		return self.created_by

	class Meta:
		verbose_name_plural = "ScrumyHistory"

# Create your models here.
