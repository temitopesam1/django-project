
from django.forms import ModelForm
from django import forms
from django.contrib.auth.forms import UserCreationForm
from .models import ScrumyGoals, ScrumyHistory, GoalStatus, User

class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    last_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')
    
    class Meta:

    	model = User
    	fields = ['first_name','last_name','email','username','password1', 'password2']



class CreateGoalForm(ModelForm):
    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(CreateGoalForm, self).__init__(*args, **kwargs)

    class Meta:
        model = ScrumyGoals
        fields = ['goal_name','user', 'goal_status']

class MoveGoalForm(ModelForm):
    class Meta:
        model = ScrumyGoals
        fields = ['goal_status']


