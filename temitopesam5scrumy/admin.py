from django.contrib import admin
from .models import ScrumyGoals
from .models import ScrumyHistory
from .models import GoalStatus

admin.site.register(ScrumyHistory)

admin.site.register(ScrumyGoals)

admin.site.register(GoalStatus)

# Register your models here.
